var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
export var Status;
(function (Status) {
    Status[Status["ERROR"] = 0] = "ERROR";
    Status[Status["SUCCESS"] = 1] = "SUCCESS";
})(Status || (Status = {}));
export var isError = function (result) {
    return result.data instanceof Error;
};
export var isSuccess = function (result) {
    return !isError(result);
};
export var fromData = function (data) { return ({
    status: data instanceof Error ? Status.ERROR : Status.SUCCESS,
    data: data
}); };
var makeSafeOperation = function (reducer) { return function (left, right) {
    if (left instanceof Error)
        return left;
    if (right instanceof Error)
        return right;
    return reducer(left, right);
}; };
export var reduce = function (operation) {
    var args = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args[_i - 1] = arguments[_i];
    }
    var initial = args[0], rest = args.slice(1);
    return rest.reduce(function (acc, current) {
        return isError(acc)
            ? acc
            : fromData(makeSafeOperation(operation)(acc.data, current));
    }, fromData(initial));
};
var rail = function (lastResult) {
    var operations = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        operations[_i - 1] = arguments[_i];
    }
    if (isError(lastResult))
        return lastResult;
    var nextOp = operations[0], restOp = operations.slice(1);
    var nextResult = fromData(nextOp(lastResult.data));
    if (restOp.length > 0)
        return rail.apply(void 0, __spreadArrays([nextResult], restOp));
    return nextResult;
};
export var railway = function (initialData) {
    var operations = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        operations[_i - 1] = arguments[_i];
    }
    var initialResult = fromData(initialData);
    if (isError(initialResult))
        return initialResult;
    if (operations.length > 0)
        return rail.apply(void 0, __spreadArrays([initialResult], operations));
    return initialResult;
};
