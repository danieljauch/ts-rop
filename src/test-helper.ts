import {Maybe} from "./index"

export const addOp = (l: number, r: number): number => l + r
export const divOp = (l: number, r: number): Maybe<number> =>
	r === 0 ? new Error("divzero") : l / r
export const iter = (n: number): number => n + 1
export const stringify = (n: number): string => "s:" + n
export const beAnError = (_input: number): Error => new Error()
