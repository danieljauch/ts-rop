import {fromData, isError, isSuccess, railway, reduce, Status} from "./index"
import {addOp, beAnError, divOp, iter, stringify} from "./test-helper"

const obj = {a: 1}
const err = new Error()

describe("fromData", () => {
	it("can properly create success from valid data", () => {
		expect(fromData(obj)).toEqual({status: Status.SUCCESS, data: obj})
	})

	it("can properly create error from invalid data", () => {
		expect(fromData(err)).toEqual({status: Status.ERROR, data: err})
	})
})

describe("isError", () => {
	it("recognizes a valid result", () => {
		expect(isError(fromData(obj))).toBe(false)
	})

	it("recognizes an error result", () => {
		expect(isError(fromData(err))).toBe(true)
	})
})

describe("isSuccess", () => {
	it("recognizes a valid result", () => {
		expect(isSuccess(fromData(obj))).toBe(true)
	})

	it("recognizes an error result", () => {
		expect(isSuccess(fromData(err))).toBe(false)
	})
})

describe("railway", () => {
	it("can make a simple operation with the same input and output type", () => {
		expect(railway(2, iter)).toEqual(fromData(3))
	})

	it("can make an operation with a different input and output type", () => {
		expect(railway(0, stringify)).toEqual(fromData("s:0"))
	})

	it("chains operations with the same types", () => {
		expect(railway(2, iter, iter, iter)).toEqual(fromData(5))
	})

	it("chains operations with different types", () => {
		expect(railway(2, iter, iter, stringify)).toEqual(fromData("s:4"))
	})

	it("can make an error type", () => {
		expect(isError(railway(0, beAnError))).toBe(true)
	})

	it("returns the first error", () => {
		const myNewError = new Error()
		const beMyError = (_input: any) => myNewError

		expect(railway(0, beMyError, beAnError)).toEqual({
			status: Status.ERROR,
			data: myNewError
		})
	})
})

describe("reduce", () => {
	it("works fine with all valid results", () => {
		expect(reduce(addOp, 1, 2, 3)).toEqual({
			status: Status.SUCCESS,
			data: 6
		})
	})

	it("becomes an error when the operation results in one", () => {
		expect(reduce(divOp, 4, 2, 0)).toEqual({
			status: Status.ERROR,
			data: Error("divzero")
		})
	})

	it("returns first invalid result", () => {
		expect(reduce((l, r) => l + r, 1, err, new Error("oops"))).toEqual({
			status: Status.ERROR,
			data: err
		})
	})
})
