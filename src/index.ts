// Utilities
export enum Status {
	ERROR,
	SUCCESS
}

export type Maybe<T> = T | Error

// Result logic
interface Result<T> {
	status: Status
	data: Maybe<T>
}
interface SucessfulResult<T> {
	status: Status.SUCCESS
	data: T
}
interface FailureResult<T> {
	status: Status.ERROR
	data: Error
}

export const isError = <T>(result: Result<T>): result is FailureResult<T> =>
	result.data instanceof Error
export const isSuccess = <T>(result: Result<T>): result is SucessfulResult<T> =>
	!isError(result)
export const fromData = <T>(data: Maybe<T>): Result<T> => ({
	status: data instanceof Error ? Status.ERROR : Status.SUCCESS,
	data
})

// Safety logic
type UnsafeReducer<T> = (left: T, right: T) => Maybe<T>
type SafeReducer<T> = (left: Maybe<T>, right: Maybe<T>) => Maybe<T>

const makeSafeOperation = <T>(reducer: UnsafeReducer<T>): SafeReducer<T> => (
	left: Maybe<T>,
	right: Maybe<T>
): Maybe<T> => {
	if (left instanceof Error) return left
	if (right instanceof Error) return right
	return reducer(left, right)
}

// Methods
export const reduce = <T>(
	operation: UnsafeReducer<T>,
	...args: Array<Maybe<T>>
): Result<T> => {
	const [initial, ...rest] = args
	return rest.reduce(
		(acc: Result<T>, current: Maybe<T>): Result<T> =>
			isError(acc)
				? acc
				: fromData(makeSafeOperation(operation)(acc.data, current)),
		fromData(initial)
	)
}

type RailOperation<T, U> = (arg: Maybe<T>) => U
const rail = (
	lastResult: Result<any>,
	...operations: Array<RailOperation<any, any>>
): Result<any> => {
	if (isError(lastResult)) return lastResult

	const [nextOp, ...restOp] = operations
	const nextResult = fromData(nextOp(lastResult.data))

	if (restOp.length > 0) return rail(nextResult, ...restOp)
	return nextResult
}
export const railway = <T>(
	initialData: T,
	...operations: Array<RailOperation<any, any>>
): Result<any> => {
	const initialResult = fromData(initialData)
	if (isError(initialResult)) return initialResult
	if (operations.length > 0) return rail(initialResult, ...operations)
	return initialResult
}
