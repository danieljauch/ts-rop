module.exports = {
  collectCoverageFrom: [
    "**/*.ts",
    "**/*.tsx",
    "!**/coverage/**",
    "!**/*.snap",
    "!**/*.json",
    "!**/*.config.js",
    "!/index.js"
  ],
  moduleFileExtensions: ["js", "jsx", "ts", "tsx"],
  reporters: ["default"],
  roots: ["<rootDir>/src"],
  setupFilesAfterEnv: ["<rootDir>/jest-setup.js"],
  testPathIgnorePatterns: [".*/node_modules", ".*/coverage"],
  testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.(j|t)sx?$",
  transform: {
    "^.+\\.(j|t)sx?$": "ts-jest"
  }
}
