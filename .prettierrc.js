module.exports = {
	arrowParens: "avoid",
	bracesSpacing: false,
	bracketSpacing: false,
	printWidth: 80,
	semi: false,
	singleQuote: false,
	tabWidth: 2,
	trailingComma: "none"
}
